import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.css']
})
export class SplashComponent implements OnInit {
  title = 'orange-bank';
  description = 'login';
  url = 'assets/images/logooficial.jpg';

  constructor() { }

  ngOnInit() {
  }

}
